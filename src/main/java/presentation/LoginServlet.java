package presentation;

import dao.LoginDAO;
import model.User;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private final static LoginDAO loginDAO = new LoginDAO(new Configuration().configure().buildSessionFactory());

    public void init() {
        // Do required initialization
    }

    public void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        final PrintWriter out = response.getWriter();

        final String username = request.getParameter("username");
        final String password = request.getParameter("password");
        final User user = loginDAO.login(username, password);
        if (user != null) {
            HttpSession oldSession = request.getSession(false);
            if (oldSession != null) {
                oldSession.invalidate();
            }
            //generate a new session
            HttpSession newSession = request.getSession(true);

            //setting session to expiry in 5 mins
            newSession.setMaxInactiveInterval(5 * 60);

            final Cookie cookie = new Cookie("isAdmin", user.getIsAdmin().toString());
            cookie.setPath("/");
            response.addCookie(cookie);
            final Cookie cookieId = new Cookie("id", user.getId().toString());
            cookieId.setPath("/");
            response.addCookie(cookieId);
            final Cookie cookieUsername = new Cookie("username", user.getUsername());
            cookieUsername.setPath("/");
            response.addCookie(cookieUsername);
            response.sendRedirect("info");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/");
            out.println("<font color=red>Either username or password is wrong.</font>");
            rd.include(request, response);
        }

        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession oldSession = request.getSession(false);
        if (oldSession != null) {
            resp.sendRedirect("info");
        }
    }

    public void destroy() {
        // do nothing.
    }
}