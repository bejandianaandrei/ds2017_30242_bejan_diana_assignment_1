package presentation;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;


public class LogoutServlet extends HttpServlet {

    public void init() {
        // Do required initialization
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        //invalidate the session if exists
        final HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
            Cookie[] cookies = request.getCookies();
            if (cookies != null)
                for (final Cookie cookie: cookies) {
                    cookie.setValue("");
                    cookie.setMaxAge(0);
                    cookie.setPath("/");

                    response.addCookie(cookie);                }
        }
        response.sendRedirect(request.getContextPath() + "/");
    }
}