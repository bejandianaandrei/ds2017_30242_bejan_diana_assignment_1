package presentation;

import bll.FlightBLL;
import bll.TimezoneBLL;
import model.City;
import model.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.List;

public class InformationServlet extends HttpServlet {

    public void init() {}

    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        // Set response content type
        response.setContentType("text/html");

        // Actual logic goes here.
        final PrintWriter out = response.getWriter();
        printHead(out);
        final HttpSession session = request.getSession(false);
        if(session != null) {
            boolean isAdmin = false;
            for (final Cookie c : request.getCookies()) {
                if (c.getName().equals("isAdmin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
            printHeader(out, isAdmin);
            printFlights(out, isAdmin);
        } else {
           printError(out);
        }
    }

    private void printHead(final PrintWriter out) {
        final String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        out.println(docType + "<html>\n<head><title> Information </title>" +
                "<style>\n" +
                "table {\n" +
                "    border-collapse: collapse;\n" +
                "    width: 100%;\n" +
                "}\n" +
                "\n" +
                "th, td {\n" +
                "    text-align: left;\n" +
                "    padding: 8px;\n" +
                "}\n" +
                "\n" +
                "tr:nth-child(even){background-color: #f2f2f2}\n" +
                "\n" +
                "th {\n" +
                "    background-color: black;\n" +
                "    color: white;\n" +
                "}\n" +
                "button {\n" +
                "    background-color: black;\n" +
                "    color: white;\n" +
                "    border: 4px solid #4C4C4C;\n" +
                "    border-radius: 12px;\n" +
                "}\n" +
                ".button:hover {\n" +
                "    background-color: #DDDDDD;\n" +
                "    color: white;\n" +
                "}" +
                "</style>" +
                "</head>\n");
    }

    private void printHeader(final PrintWriter out, final boolean isAdmin) {
        if (isAdmin) {
            out.println("<div class=\"header\">");
            out.println("<table><tr><td>");
            out.println("<form action=\"flights\" method=\"GET\" align=\"left\">");
            out.println("<button>CreateFlight</button></form>");
            out.println("</td><td>");
            out.println("<form action=\"logout\" method=\"GET\" align=\"right\">");
            out.println("<button>LogOut</button></form>");
            out.println("</div>");
            out.println("</td></tr></table>");


        } else {
            out.println("<div class=\"header\">");
            out.println("<form action=\"logout\" method=\"GET\" align=\"right\">");
            out.println("<button>LogOut</button>></form>");
            out.println("</div>");
        }
        out.println("<table><tr>");
    }

    private void printFlights(final PrintWriter out, final boolean isAdmin) {
        final FlightBLL flightBLL = new FlightBLL();
        final TimezoneBLL timezoneBLL = new TimezoneBLL();
        final List<Flight> flights = flightBLL.getAll();

        out.println("<body bgcolor = \"#f0f0f0\">\n");

        for(final Field field: Flight.class.getDeclaredFields()) {
            out.println("<th>" + field.getName().toUpperCase() + " </th>");
        }
        out.println("<th>LOCAL TIME DEPARTURE</th>");

        out.println("</tr>");
        for(final Flight flight: flights) {
            out.println("<tr>");
            if(isAdmin)
                out.println("<td><a href=\"flights?id=" + flight.getFlightNumber() + " \">" + flight.getFlightNumber() + "</a></td>");
            else
                out.println("<td>" + flight.getFlightNumber() + "</td>");
            out.println("<td>" + flight.getAirplaneType() + " </td>");
            out.println("<td>" + flight.getDepartureDate() + " </td>");
            out.println("<td>" + flight.getDepartureHour() + " </td>");
            out.println("<td>" + flight.getDepartureCity() + " </td>");
            out.println("<td>" + flight.getArrivalDate() + " </td>");
            out.println("<td>" + flight.getArrivalHour() + " </td>");
            out.println("<td>" + flight.getArrivalCity() + " </td>");
            final City departureCity= flight.getDepartureCity();
            out.println("<td>" + timezoneBLL.getLocalTime(departureCity.getLat(), departureCity.getLon()) + " </td>");
            out.println("</tr>");
        }
        out.println("</table></body>\n</html>");
    }

    private void printError(final PrintWriter out) {

        out.println("<div style=\"color:red;\">Access denied, please login</div>" +
                "<div><form action=\"/tema1\" method=\"GET\">" +
                "<button>Login</button>\n</form> </div>");
    }
    public void destroy() {}
}