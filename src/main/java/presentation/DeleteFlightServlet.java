package presentation;

import bll.FlightBLL;
import util.MessagePrinter;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class DeleteFlightServlet extends HttpServlet {

    public void init() {
        // Do required initialization
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        final PrintWriter out = response.getWriter();
        final HttpSession session = request.getSession(false);
        if (session != null) {
            boolean isAdmin = false;
            for (final Cookie c : request.getCookies()) {
                if (c.getName().equals("isAdmin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
            if (isAdmin) {
                final String flightNumber = request.getParameter("id");
                new FlightBLL().delete(Integer.parseInt(flightNumber), isAdmin);

                response.sendRedirect("/tema1/info");

            } else {
                MessagePrinter.printErrorAccessDenied(out);
            }
        } else {
            MessagePrinter.printErrorAccessDenied(out);
        }
    }
}