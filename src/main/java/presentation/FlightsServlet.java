package presentation;

import bll.CityBLL;
import bll.FlightBLL;
import model.Flight;
import util.MessagePrinter;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;

public class FlightsServlet extends HttpServlet {

    public void init() {
        // Do required initialization
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        final String sid = request.getParameter("id");
        final PrintWriter out = response.getWriter();
        final HttpSession session = request.getSession(false);
        if (session != null) {
            boolean isAdmin = false;
            for (final Cookie c : request.getCookies()) {
                if (c.getName().equals("isAdmin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
            MessagePrinter.printHead(out);
            if (isAdmin) {
                final int id = (sid == null || sid.isEmpty() || !sid.toLowerCase().equals(sid.toUpperCase())) ?
                        0 : Integer.parseInt(sid);
                final Flight flight = (id == 0) ? null : new FlightBLL().get(id);
                out.println("<body>");
                if(flight == null)
                    out.println("<table><tr><td align=\"left\"><form action=\"flights\" method=\"POST\">");
                else
                    out.println("<form action=\"editFlight\" method=\"POST\">");
                out.println("<table><tr><th>Name</th><th>Value</th></tr>");
                out.println("<tr><td>Flight Number</td>");
                final String flightNumber = (flight == null) ? "" : flight.getFlightNumber().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"flightNumber\" class=\"form-control\" disabled " +
                        "id=\"flightNumber\" value=\"" + flightNumber + "\">\n</div></td></tr>");
                out.println("<tr><td>Airplane Type</td>");
                final String airplaneType = (flight == null) ? "" : flight.getAirplaneType();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"airplaneType\" class=\"form-control\" " +
                        "id=\"airplaneType\" value=\"" + airplaneType + "\">\n</div></td></tr>");
                out.println("<tr><td>Departure City</td>");
                final String departureCity = (flight == null) ? "" : flight.getDepartureCity().getId().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"departureCity\" class=\"form-control\" " +
                        "id=\"departureCity\" value=\"" + departureCity + "\">\n</div></td></tr>");
                out.println("<tr><td>Departure Date</td>");
                final String departureDate = (flight == null) ? "YYYY-MM-DD" : flight.getDepartureDate().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"departureDate\" class=\"form-control\" " +
                        "id=\"departureDate\" value=\"" + departureDate + "\">\n</div></td></tr>");
                out.println("<tr><td>Departure Hour</td>");
                final String departureHour = (flight == null) ? "hh:mm:ss" : flight.getDepartureHour().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"departureHour\" class=\"form-control\" " +
                        "id=\"departureHour\" value=\"" + departureHour + "\">\n</div></td></tr>");
                out.println("<tr><td>Arrival City</td>");
                final String arrivalCity = (flight == null) ? "" : flight.getArrivalCity().getId().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"arrivalCity\" class=\"form-control\" " +
                        "id=\"arrivalCity\" value=\"" + arrivalCity + "\">\n</div></td></tr>");
                out.println("<tr><td>Arrival Date</td>");
                final String arrivalDate = (flight == null) ? "YYYY-MM-DD" : flight.getArrivalDate().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"arrivalDate\" class=\"form-control\" " +
                        "id=\"arrivalDate\" value=\"" + arrivalDate + "\">\n</div></td></tr>");
                out.println("<tr><td>Arrival Hour</td>");
                final String arrivalHour = (flight == null) ? "hh:mm:ss" : flight.getArrivalHour().toString();
                out.println("<td><div class=\"form-group\">\n" +
                        "<input name=\"arrivalHour\" class=\"form-control\" " +
                        "id=\"arrivalHour\" value=\"" + arrivalHour + "\">\n</div></td></tr>");
                out.println("</table>\n");

                if(id==0)
                {
                    out.println("<button>Create</button></td></tr></table></form>");
                }
                else {
                    out.println("<button>Edit</button></form></td><td align=\"right\">");
                    out.println("<form action=\"deleteFlight\" method=\"POST\">");
                    out.println("<input hidden name=\"id\" value=\"" + flight.getFlightNumber().toString() + "\">");
                    out.println("<button>Delete</button></form></td></tr></table>");
                }
                    out.println("</body>\n");
            } else {
                MessagePrinter.printErrorAccessDenied(out);
            }

        } else {
            MessagePrinter.printErrorAccessDenied(out);
        }

    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final FlightBLL flightsService = new FlightBLL();
        final CityBLL cityService = new CityBLL();
        boolean isAdmin = false;

        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("isAdmin") && c.getValue().equals("true")) {
                    isAdmin = true;
                }
            }
        }
        final PrintWriter out = response.getWriter();
        if (!isAdmin) {
            MessagePrinter.printErrorAccessDenied(out);
        }

        final Flight flight = new Flight();
        flight.setAirplaneType(request.getParameter("airplaneType"));
        flight.setDepartureCity(cityService.get(Integer.parseInt(request.getParameter("departureCity"))));
        flight.setDepartureDate(Date.valueOf(request.getParameter("departureDate")));
        flight.setDepartureHour(Time.valueOf(request.getParameter("departureHour")));
        flight.setArrivalCity(cityService.get(Integer.parseInt(request.getParameter("arrivalCity"))));
        flight.setArrivalDate(Date.valueOf(request.getParameter("arrivalDate")));
        flight.setArrivalHour(Time.valueOf(request.getParameter("arrivalHour")));
        try {
            flight.setFlightNumber(Integer.parseInt(request.getParameter("flightNumber")));
        } catch (final Exception e) {}

        flightsService.add(flight, isAdmin);

        response.sendRedirect("/tema1/info");

    }

}
