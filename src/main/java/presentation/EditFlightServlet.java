package presentation;

import bll.CityBLL;
import bll.FlightBLL;
import model.Flight;
import util.MessagePrinter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;

public class EditFlightServlet extends HttpServlet {

    public void init() {
        // Do required initialization
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final FlightBLL flightsService = new FlightBLL();
        final CityBLL cityService = new CityBLL();
        boolean isAdmin = false;

        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("isAdmin") && c.getValue().equals("true")) {
                    isAdmin = true;
                }
            }
        }
        final PrintWriter out = response.getWriter();
        if (!isAdmin) {
            MessagePrinter.printErrorAccessDenied(out);
        }

        final Flight flight = new Flight();
        flight.setFlightNumber(Integer.parseInt(request.getParameter("flightNumber")));
        flight.setAirplaneType(request.getParameter("airplaneType"));
        flight.setDepartureCity(cityService.get(Integer.parseInt(request.getParameter("departureCity"))));
        flight.setDepartureDate(Date.valueOf(request.getParameter("departureDate")));
        flight.setDepartureHour(Time.valueOf(request.getParameter("departureHour")));
        flight.setArrivalCity(cityService.get(Integer.parseInt(request.getParameter("arrivalCity"))));
        flight.setArrivalDate(Date.valueOf(request.getParameter("arrivalDate")));
        flight.setArrivalHour(Time.valueOf(request.getParameter("arrivalHour")));

        flightsService.edit(flight, isAdmin);
        response.sendRedirect("/tema1/info");
    }
}
