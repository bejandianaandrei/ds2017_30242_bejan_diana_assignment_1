package util;

import java.io.PrintWriter;

public class MessagePrinter {

    private MessagePrinter(){}

    public static void printHead(final PrintWriter out) {
        final String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        out.println(docType + "<html>\n<head><title> Information </title>" +
                "<style>\n" +
                "table {\n" +
                "    border-collapse: collapse;\n" +
                "    width: 100%;\n" +
                "}\n" +
                "\n" +
                "th, td {\n" +
                "    text-align: left;\n" +
                "    padding: 8px;\n" +
                "}\n" +
                "\n" +
                "tr:nth-child(even){background-color: #f2f2f2}\n" +
                "\n" +
                "th {\n" +
                "    background-color: black;\n" +
                "    color: white;\n" +
                "}\n" +
                "button, input {\n" +
                "    background-color: black;\n" +
                "    color: white;\n" +
                "    border: 4px solid #4C4C4C;\n" +
                "    border-radius: 12px;\n" +
                "}\n" +
                ".button:hover {\n" +
                "    background-color: #DDDDDD;\n" +
                "    color: white;\n" +
                "}" +
                "</style>" +
                "</head>\n");
    }

    public static void printErrorAccessDenied(final PrintWriter out) {
        out.println("<div style=\"color:red;\">Access denied, please login or contact your administrator</div>" +
                "<div><form action=\"/tema1\" method=\"GET\">" +
                "<button>Login</button>\n</form> </div>");
    }
}
