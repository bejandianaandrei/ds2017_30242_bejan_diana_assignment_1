package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name = "flight")
public class Flight {

    @Id @GeneratedValue(strategy = GenerationType.AUTO,
    generator = "native")
    @GenericGenerator(name = "native",
    strategy = "native")
    @Column(name = "flightNumber")
    private Integer flightNumber;
    @Column(name = "airplaneType")
    private String airplaneType;
    @Column(name = "departureDate")
    private Date departureDate;
    @Column(name = "departureHour")
    private Time departureHour;
    @ManyToOne
    @JoinColumn(name = "departureCity")
    private City departureCity;
    @Column(name = "arrivalDate")
    private Date arrivalDate;
    @Column(name = "arrivalHour")
    private Time arrivalHour;
    @ManyToOne
    @JoinColumn(name = "arrivalCity")
    private City arrivalCity;

    public Flight() {
    }

    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(final Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(final String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(final Date departureDate) {
        this.departureDate = departureDate;
    }

    public Time getDepartureHour() {
        return departureHour;
    }

    public void setDepartureHour(final Time departureTime) {
        this.departureHour = departureTime;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(final City departureCity) {
        this.departureCity = departureCity;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(final Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Time getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(final Time arrivalTime) {
        this.arrivalHour = arrivalTime;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(final City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }
}
