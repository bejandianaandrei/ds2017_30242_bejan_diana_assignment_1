package model;

import javax.persistence.*;

/**
 * Class stores information about geographical location of a city.
 */
@Entity
@Table(name = "city")
public class City {

    @Id @GeneratedValue
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "lat")
    private Float lat;
    @Column(name = "lon")
    private Float lon;

    public City() {
    }

    public City(final String name, final Float lat, final Float lon) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    public City(final Integer id, final String name, final Float lat, final Float lon) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(final Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(final Float lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return name;
    }
}
