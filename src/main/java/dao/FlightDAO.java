package dao;

import model.Flight;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public class FlightDAO {

    private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);

    private SessionFactory factory;

    public FlightDAO(final SessionFactory factory) {
        this.factory = factory;
    }

    public void add(final Flight flight) {
        final Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public void update(final Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Flight> selectAll() {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight ").list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
            session.close();
        }
        return flights;
    }

    @SuppressWarnings("unchecked")
    public Flight find(final int flightNumber) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public Flight delete(final int flightNumber) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        Flight flight = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            flight  =  flights != null && !flights.isEmpty() ? flights.get(0) : null;
            session.delete(flight);
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return flight;
    }
}