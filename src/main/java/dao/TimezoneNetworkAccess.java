package dao;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class TimezoneNetworkAccess {

    public String getLocalTime(final double latitude, final double longitude) throws Exception {
        final String url = "http://www.new.earthtools.org/timezone-1.1/" + latitude + "/" + longitude;

        final URL obj = new URL(url);
        final HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        final int responseCode = con.getResponseCode();
        if (responseCode != 200) {
            return "";
        }

        final BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        final StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        final String xmlData = response.toString();

        final SAXBuilder saxBuilder = new SAXBuilder();
        final InputStream inputStream = new ByteArrayInputStream(xmlData.getBytes());
        final Document document = saxBuilder.build(inputStream);
        final Element rootNode = document.getRootElement();
        return rootNode.getChildText("localtime");
    }
}