package dao;

import model.City;
import model.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

public class CityDAO {

    private static final Log LOGGER = LogFactory.getLog(CityDAO.class);

    private SessionFactory factory;

    public CityDAO(final SessionFactory factory) {
        this.factory = factory;
    }

    public City add(final City city) {
        int cityId = -1;
        final Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            cityId = (Integer) session.save(city);
            city.setId(cityId);
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return city;
    }

    @SuppressWarnings("unchecked")
    public City find(final int id) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            cities = query.list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }
}
