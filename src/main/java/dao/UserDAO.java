package dao;

import model.User;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public class UserDAO {

    private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

    private SessionFactory factory;

    public UserDAO(final SessionFactory factory) {
        this.factory = factory;
    }

    public User add(final User user) {
        int id = -1;
        final Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            id = (Integer) session.save(user);
            user.setId(id);
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> selectAll() {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            users = session.createQuery("FROM User ").list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
            session.close();
        }
        return users;
    }

    @SuppressWarnings("unchecked")
    public User find(final int id) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM User WHERE id = :id");
            query.setParameter("id", id);
            users = query.list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public User find(final String username) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            users = query.list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public User delete(final int id) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM User WHERE id = :id");
            query.setParameter("id", id);
            users = query.list();
            user  =  users != null && !users.isEmpty() ? users.get(0) : null;
            session.delete(user);
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return user;
    }
}