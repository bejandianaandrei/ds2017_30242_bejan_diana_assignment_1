package dao;

import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;

public class LoginDAO {

    private static final Log LOGGER = LogFactory.getLog(LoginDAO.class);

    private SessionFactory factory;

    public LoginDAO(final SessionFactory factory) {
        this.factory = factory;
    }

    public User login(final String username, final String password) {
        final Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            final Query query = session.createQuery("FROM User WHERE username = :username AND password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            users = query.list();
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error(e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }
}
