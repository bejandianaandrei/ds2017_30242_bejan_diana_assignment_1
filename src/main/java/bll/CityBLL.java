package bll;

import dao.CityDAO;
import model.City;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CityBLL {

    private CityDAO cityDAO;

    public CityBLL() {
        final SessionFactory sessionFactory =  new Configuration().configure().buildSessionFactory();
        cityDAO = new CityDAO(sessionFactory);
    }

    public City get(final int id) {
        return cityDAO.find(id);
    }
}
