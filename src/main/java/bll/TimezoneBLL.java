package bll;

import dao.TimezoneNetworkAccess;

public class TimezoneBLL {

    public String getLocalTime(final double latitude, final double longitude) {
        try {
            return new TimezoneNetworkAccess().getLocalTime(latitude, longitude);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return "ERROR";
    }
}
