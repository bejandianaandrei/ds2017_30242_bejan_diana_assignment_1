package bll;

import dao.FlightDAO;
import model.Flight;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class FlightBLL {
    private FlightDAO flightDAO;

    public FlightBLL() {
        final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        flightDAO = new FlightDAO(sessionFactory);
    }

    public List<Flight> getAll() {
        return flightDAO.selectAll();
    }

    public Flight get(final int flightId) {
        return flightDAO.find(flightId);
    }

    public void add(final Flight flight, final boolean hasRights) {
        if (hasRights) {
           flightDAO.add(flight);
        }
    }

    public void edit(final Flight flight, final boolean hasRights) {

        if (hasRights) {
            flightDAO.update(flight);
        }
    }

    public void delete(final int flightNumber, final boolean hasRights) {

        if (hasRights) {
            flightDAO.delete(flightNumber);
        }
    }
}