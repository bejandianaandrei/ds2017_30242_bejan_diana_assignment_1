package dao;

import model.User;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;


public class LoginDAOTest {

    @org.junit.Test
    public void login() {
        LoginDAO loginDAO = new LoginDAO(new Configuration().configure().buildSessionFactory());
        User login = loginDAO.login("diana", "diana");
        Assert.assertNotNull(login);
    }
}