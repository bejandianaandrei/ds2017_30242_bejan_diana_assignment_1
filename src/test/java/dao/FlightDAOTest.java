package dao;

import bll.CityBLL;
import model.Flight;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.sql.Time;


public class FlightDAOTest {

    @Test
    public void add() {
        final Flight flight = new Flight();
        final CityBLL cityBLL = new CityBLL();
        final FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        final int count = flightDAO.selectAll().size();
        flight.setDepartureCity(cityBLL.get(1));
        flight.setArrivalCity(cityBLL.get(1));
        flight.setArrivalHour(Time.valueOf("12:00:00"));
        flight.setDepartureHour(Time.valueOf("12:00:00"));
        flight.setArrivalDate(Date.valueOf("1996-12-12"));
        flight.setDepartureDate(Date.valueOf("1996-12-12"));
        flight.setAirplaneType("kkt");
        flight.setFlightNumber(23);
        flightDAO.add(flight);
        Assert.assertEquals(count, flightDAO.selectAll().size() - 1);
    }

    @Test
    public void edit() {
        final Flight flight = new Flight();
        final CityBLL cityBLL = new CityBLL();
        final FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        final int count = flightDAO.selectAll().size();
        flight.setDepartureCity(cityBLL.get(1));
        flight.setArrivalCity(cityBLL.get(1));
        flight.setArrivalHour(Time.valueOf("12:00:00"));
        flight.setDepartureHour(Time.valueOf("12:00:00"));
        flight.setArrivalDate(Date.valueOf("1996-12-12"));
        flight.setDepartureDate(Date.valueOf("1996-12-12"));
        flight.setAirplaneType("rfuyhtgrferty");
        flight.setFlightNumber(23);
        flightDAO.update(flight);
        Assert.assertEquals(count, flightDAO.selectAll().size());
    }
}