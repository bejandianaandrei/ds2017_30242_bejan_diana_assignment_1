CREATE DATABASE  IF NOT EXISTS flights;
USE flights;

DROP TABLE IF EXISTS user;
CREATE TABLE user(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(40) NOT NULL,
    lastname VARCHAR(40) NOT NULL,
    username VARCHAR(40) NOT NULL,
	password VARCHAR(40) NOT NULL,
    isAdmin BOOL DEFAULT FALSE
);

DROP TABLE IF EXISTS flight;
DROP TABLE IF EXISTS city;
CREATE TABLE city(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(50),
lat FLOAT,
lon FLOAT
);

CREATE TABLE flight (
    flightNumber INT NOT NULL PRIMARY KEY,
    airplaneType VARCHAR(40) NOT NULL,
	departureDate DATE NOT NULL,
    departureHour TIME NOT NULL,
	departureCity INT NOT NULL ,
	arrivalDate DATE NOT NULL,
    arrivalHour TIME NOT NULL,
    arrivalCity INT NOT NULL,
    FOREIGN KEY (departureCity) REFERENCES city(id),
	FOREIGN KEY (arrivalCity) REFERENCES city(id)
);
alter table flight modify flightNumber int auto_increment; 